# Sources de photos.marienfressinaud.fr

Ce dépôt contient les sources de mon site de photos : [photos.marienfressinaud.fr](https://photos.marienfressinaud.fr)

Le site est généré à l’aide de [Boop! Photos](https://framagit.org/marienfressinaud/boop-photos),
mon générateur de sites statiques pour photos. Pour générer le site, il faut
donc l’installer au préalable.

Les commandes utiles pour générer et publier ce site sont déclarées dans le
fichier [`Makefile`](/Makefile).

## Travail en local

Pour avoir un aperçu global des fichiers sources :

```console
$ make tree
```

Pour générer le site en local :

```console
$ make boop
```

Pour ouvrir le site dans le navigateur :

```console
$ make serve
```

## Mise en production

Avant de publier, il faut au préalable définir la variable d’environnement qui
va bien, dans un fichier `.env` par exemple :

```dotenv
SERVER_DEST=user@server.url:/path/to/website
```

Pour publier le site sur le serveur :

```console
$ make publish
```

## Licence

Sauf mention contraire, le contenu du site est placé sous licence [CC BY
4.0](https://creativecommons.org/licenses/by/4.0/). Vous pouvez me créditer en
indiquant l’adresse de mon site ([photos.marienfressinaud.fr](https://photos.marienfressinaud.fr)).
