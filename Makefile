.DEFAULT_GOAL := help

# SERVER_DEST is defined in a `.env` file and follow this format: user@server.url:/path/to/website
include .env

.PHONY: boop
boop: ## Build the website
	boop-photos.py

.PHONY: clean
clean: ## Clean output files
	rm -rf ./_site

.PHONY: serve
serve: boop ## Serve the website (development)
	cd _site && python3 -m http.server 8080

.PHONY: publish
publish: boop  ## Publish the website online (rsync)
	rsync -P -rvzc --cvs-exclude --delete ./_site/ $(SERVER_DEST)

.PHONY: tree
tree:  ## Display the structure of the website
	tree -I '_site' --dirsfirst -CA

.PHONY: help
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
