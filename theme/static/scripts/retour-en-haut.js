(function () {
    var backToTopContainer = document.querySelector('#la-haut');
    var intersectionObserver = new IntersectionObserver(function (entries) {
        entries.forEach(function (entry) {
            entry.target.classList.toggle('entrecoupant', entry.isIntersecting);
        });
    });
    intersectionObserver.observe(backToTopContainer);

    var backToTopButton = backToTopContainer.querySelector('button');
    backToTopButton.onclick = function() {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };
}());
